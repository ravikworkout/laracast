<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * List all the users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function users()
    {
          $users = DB::table('users')->paginate(15);
        return view('users',['users' => $users]);
    }


        /**
     * List all the users.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
    return view('users.edit', ['user' => User::findOrFail($id)]);
    }
}
